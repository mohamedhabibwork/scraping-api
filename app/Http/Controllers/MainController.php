<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use NigelCunningham\Puphpeteer\Puppeteer;
use NigelCunningham\Rialto\Data\JsFunction;

class MainController extends Controller
{
    public static $res;

    public static function index(Request $req)
    {
        $email = "facescrpe@gmail.com";
        $password = "aaaaasssss";
        $searchLimit = $req->searchLimit;
        $sort = $req->sort;
        $keyword = $req->keyword;
        try {
            return match ($sort) {
                "videos" => static::facebookVideosScraping($searchLimit, $keyword, $email, $password),
                "posts" => static::facebookPostsScraping($searchLimit, $keyword, $email, $password),
                "latest_posts" => static::facebookLatestPostsScraping($searchLimit, $keyword, $email, $password),
                default => static::facebookVideosScraping($searchLimit, $keyword, $email, $password),
            };
        } catch (\Throwable $exception) {
            report($exception);
            return [
                "error" => $exception->getMessage(),
                "trace" => $exception->getTrace(),
            ];
        }
    }

    public static function facebookVideosScraping($searchLimit, $keyword, $email, $password)
    {
        $puppeteer = new Puppeteer;

        $browser = $puppeteer->launch([
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
        ]);
        $page = $browser->newPage();
        $page->goto("https://www.facebook.com/");
        $page->type('input[name=email]', $email);
        $page->type("input[name=pass]", $password);
        $page->waitForSelector("button[type=submit]");
        $page->click("button[type=submit]");
        $page->waitForNavigation();
        $tmp = $page->evaluate(JSFunction::createWithBody("
        if(document.querySelector('input[name=email]')) {
            return 0;
        }
        return 1;
        "
        ));
        if ($tmp == 0) {
            return [
                "error" => "Unauthenticated"
            ];
        }

        $page->goto("https://www.facebook.com/search/videos/?q=" . $keyword);


        $sel = "div[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz xt0b8zv xzsf02u x1s688f']";

        $count = 0;
        $sdata = [];
        // page.on('console', async (msg) => {
        //     const msgArgs = msg.args();
        //     for (let i = 0; i < msgArgs.length; ++i) {
        //         console.log(await msgArgs[i].jsonValue());
        //     }
        // });

        while ($count < $searchLimit) {
            $x = $page->evaluate(JSFunction::createWithBody("

        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
            window.scrollBy(0, 100);
            if (document.querySelector(\"[role=feed] [class='x1n2onr6 x1ja2u2z x9f619 x78zum5 xdt5ytf x2lah0s x193iq5w xz9dl7a']\")) {
                return 0;
            }
            if(document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/y_/r/Krj1JsX3uTI.svg?_nc_eui2=AeFlHmecVHfBL0GiEFOTScAfejgaiNC5All6OBqI0LkCWcWpgMfkyH4KFDta9BQPSGrdYyY4wuznAO3z7VlIjomN']\")) {
                return 0;
            }

            return 1;
        "));

            if ($x == 0) {
                break;
            }

            sleep(0.2);

            $page->evaluate(JSFunction::createWithParameters(['sel'])->body("
            let elements = Array.from(document.querySelectorAll(sel));
            elements.forEach((item) => {
                item.click();
            });
        "));

            $f = $page->evaluate(JSFunction::createWithBody("
                let all_data = [];
                all_data = Array.from(document.querySelectorAll(\"[role=article]\")).map((item) => {
                    let data = {};
                    let title = item.querySelector(\"a[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz x1heor9g xt0b8zv x1s688f']\")
                    if(title == null) {
                        return null;
                    }
                    data.text = title.textContent;
                    data.url = title.href;
                    return data;
                })
                all_data = all_data.filter((item) => {
                    return item != null;
                });
                let count = all_data.length;
                return {count, sdata: all_data};

            // console.log(sdata);
            // console.log(\"count=\", count);
            "
            ));
            $count = $f['count'];
            $sdata = $f['sdata'];
        }
        $browser->close();
        return $sdata;
    }

    public static function facebookPostsScraping($searchLimit, $keyword, $email, $password)
    {
        // console.log(req.body.searchLimit);
        // console.log(req.body.keyword);
        $puppeteer = new Puppeteer;
        $browser = $puppeteer->launch([
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
        ]);
        $page = $browser->newPage();
        $page->goto("https://www.facebook.com/");
        $page->type('input[name=email]', $email);
        $page->type("input[name=pass]", $password);
        $page->waitForSelector("button[type=submit]");
        $page->click("button[type=submit]");
        $page->waitForNavigation();
        $tmp = $page->evaluate(JSFunction::createWithBody("
        if(document.querySelector(\"input[name=email]\")) {
            return 0;
        }
        return 1;
    "));
        if ($tmp == 0) {
            return [
                "error" => "Unauthenticated"
            ];
        }
        $page->goto("https://www.facebook.com/search/posts?q=" . $keyword);

        $sel = "div[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz xt0b8zv xzsf02u x1s688f']";

        $count = 0;
        $sdata = [];
        // page.on('console', async (msg) => {
        //     const msgArgs = msg.args();
        //     for (let i = 0; i < msgArgs.length; ++i) {
        //     console.log(await msgArgs[i].jsonValue());
        //     }
        // });

        while ($count < $searchLimit) {
            $x = $page->evaluate(JSFunction::createWithBody("
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }
            window.scrollBy(0, 100);
            if (document.querySelector(\"[role=feed] [class='x1n2onr6 x1ja2u2z x9f619 x78zum5 xdt5ytf x2lah0s x193iq5w xz9dl7a']\")) {
                return 0;
            }
            if(document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/y_/r/Krj1JsX3uTI.svg?_nc_eui2=AeFlHmecVHfBL0GiEFOTScAfejgaiNC5All6OBqI0LkCWcWpgMfkyH4KFDta9BQPSGrdYyY4wuznAO3z7VlIjomN']\")) {
                return 0;
            }

            return 1;
        "));

            if ($x == 0) {
                break;
            }

            sleep(0.2);

            $page->evaluate(JSFunction::createWithParameters(["sel"])->body("
            let elements = Array.from(document.querySelectorAll(sel));
            elements.forEach((item) => {
                item.click();
            });
        "));

            $f = $page->evaluate(JSFunction::createWithBody("
        let all_data = [];
        all_data = Array.from(document.querySelectorAll(\"[role=article]\")).map((item) => {
            let data = {};
            let first_part = item.querySelector(\":scope > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(2)\");
            let second_part = item.querySelector(\":scope > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(3)\");
            data.text = '';
            if(second_part == null) {
                return null;
            }
            if(first_part == null) {
                return null;
            }
            let title = first_part.querySelector(\"h3[id] a\");
            let subtitles = Array.from(first_part.querySelectorAll(\"span[id] span:not([class]) a\"));
            if(subtitles.length > 1) {
                data.profile_link = subtitles[0].href;
                data.post_link = subtitles[1].href;
            } else if(subtitles.length == 1){
                data.post_link = subtitles[0].href;
                data.profile_link = title.href;
            } else {
                data = null;
                return data;
            }
            if(second_part.children.length > 0 && !second_part.children[0].hasAttribute('id')) {
                if(second_part.children[0].getAttribute('class') != '') {
                    if(second_part.children[0].children[0].querySelector(\":scope > div[dir=auto]\") == null) {
                        return null;
                    }
                    data.text += second_part.children[0].children[0].querySelector(\":scope > div[dir=auto]\").textContent;
                } else {
                    if(second_part.children[0] == null) {
                        return null;
                    }
                    data.text += second_part.children[0].textContent;
                }
            }
            if(second_part.children.length > 1 && !second_part.children[1].hasAttribute('id')) {
                if(second_part.children[1].getAttribute('class') != '') {
                    if(second_part.children[1].children[0].querySelector(\":scope > div[dir=auto]\") == null) {
                        return null;
                    }
                    data.text += second_part.children[1].children[0].querySelector(\":scope > div[dir=auto]\").textContent;
                } else {
                    if(second_part.children[1] == null) {
                        return null;
                    }
                    data.text += second_part.children[1].textContent;
                }
            }

            return data;
        })
        all_data = all_data.filter(item => {
            return item != null;
        });

        let count = all_data.length;
        return {count, sdata: all_data};
        "));
            $sdata = $f['sdata'];
            $count = $f['count'];
        }
        $page->close();
        $xdata = [];
        foreach ($sdata as $item) {
            // console.log("itemmmmmmmmmmmm=", item);
            $page = $browser->newPage();
            $page->goto($item["profile_link"]);

            $city = $page->evaluate(JSFunction::createWithBody("
            let e = document.querySelector(\"div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 x2lah0s x1qughib x6s0dn4 xozqiw3 x1q0g3np'] > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x2lah0s x193iq5w xeuugli']\");
            if(e) {
                e.click();
            }
            let x = document.querySelector(\"div[class='x78zum5 xdt5ytf x1iyjqo2 x1n2onr6'] > div[class='x4k7w5x x1h91t0o x1beo9mf xaigb6o x12ejxvf x3igimt xarpa2k xedcshv x1lytzrv x1t2pt76 x7ja8zs x1n2onr6 x1qrby5j x1jfb8zj'] a\");
            if(x) {
                x.click();
            }
            let y = document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/v3/y5/r/VMZOiSIJIwn.png?_nc_eui2=AeF83LbZBKKn3uYtYjn3OcyGysO07LK9kRPKw7Tssr2RE77QWzkWrBNk-fIyTJluVdn2ltA6S71UD8ATh6jI2L4u']\");
            if(y){
                if(y.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\") == null) {
                    throw new Error('dd5');
                }
                return y.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\").textContent;
            }
            let z = document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/v3/yW/r/8k_Y-oVxbuU.png?_nc_eui2=AeFxXyXIUKzKuNmXH1nUn0XWAMO_1wPOzQ4Aw7_XA87NDhIikw3fYmgZgfgWF15jqCf6YZw9AnJZxaSSvinlQqO9']\");
            if(z) {
                if(z.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\") == null) {
                    throw new Error('dd6');
                }
                return z.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\").textContent;
            }
            return '';
        "));
            $page->close();
            $xdata[] = [
                ...$item,
                "city" => $city
            ];
        }
        $browser->close();
        return $xdata;
    }

    public static function facebookLatestPostsScraping($searchLimit, $keyword, $email, $password)
    {
        // console.log(req.body.searchLimit);
        // console.log(req.body.keyword);
        $puppeteer = new Puppeteer;
        $browser = $puppeteer->launch([
            'args' => ['--no-sandbox', '--disable-setuid-sandbox'],
        ]);
        $page = $browser->newPage();
        $page->goto("https://www.facebook.com/");
        $page->type('input[name=email]', $email);
        $page->type("input[name=pass]", $password);
        $page->waitForSelector("button[type=submit]");
        $page->click("button[type=submit]");
        $page->waitForNavigation();
        $tmp = $page->evaluate(JSFunction::createWithBody("
        if(document.querySelector(\"input[name=email]\")) {
            return 0;
        }
        return 1;
    "));
        if ($tmp == 0) {
            return [
                "error" => "Unauthenticated"
            ];
        }
        $page->goto("https://www.facebook.com/search/posts?q=" . $keyword . "&filters=eyJyZWNlbnRfcG9zdHM6MCI6IntcIm5hbWVcIjpcInJlY2VudF9wb3N0c1wiLFwiYXJnc1wiOlwiXCJ9In0%3D");

        $sel = "div[class='x1i10hfl xjbqb8w x6umtig x1b1mbwd xaqea5y xav7gou x9f619 x1ypdohk xt0psk2 xe8uvvx xdj266r x11i5rnm xat24cr x1mh8g0r xexx8yu x4uap5 x18d9i69 xkhd6sd x16tdsg8 x1hl2dhg xggy1nq x1a2a7pz xt0b8zv xzsf02u x1s688f']";

        $count = 0;
        $sdata = [];
        // page.on('console', async (msg) => {
        //     const msgArgs = msg.args();
        //     for (let i = 0; i < msgArgs.length; ++i) {
        //     console.log(await msgArgs[i].jsonValue());
        //     }
        // });

        while ($count < $searchLimit) {
            $x = $page->evaluate(JSFunction::createWithBody("
            function sleep(ms) {
                return new Promise(resolve => setTimeout(resolve, ms));
            }
            window.scrollBy(0, 100);
            if (document.querySelector(\"[role=feed] [class='x1n2onr6 x1ja2u2z x9f619 x78zum5 xdt5ytf x2lah0s x193iq5w xz9dl7a']\")) {
                return 0;
            }
            if(document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/y_/r/Krj1JsX3uTI.svg?_nc_eui2=AeFlHmecVHfBL0GiEFOTScAfejgaiNC5All6OBqI0LkCWcWpgMfkyH4KFDta9BQPSGrdYyY4wuznAO3z7VlIjomN']\")) {
                return 0;
            }

            return 1;
        "));

            if ($x == 0) {
                break;
            }

            sleep(0.2);

            $page->evaluate(JSFunction::createWithParameters(["sel"])->body("
            let elements = Array.from(document.querySelectorAll(sel));
            elements.forEach((item) => {
                item.click();
            });
        "));

            $f = $page->evaluate(JSFunction::createWithBody("
        let all_data = [];
        all_data = Array.from(document.querySelectorAll(\"[role=article]\")).map((item) => {
            let data = {};
            let first_part = item.querySelector(\":scope > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(2)\");
            let second_part = item.querySelector(\":scope > div > div > div > div > div > div:nth-child(2) > div > div > div:nth-child(3)\");
            data.text = '';
            if(second_part == null) {
                return null;
            }
            if(first_part == null) {
                return null;
            }
            let title = first_part.querySelector(\"h3[id] a\");
            let subtitles = Array.from(first_part.querySelectorAll(\"span[id] span:not([class]) a\"));
            if(subtitles.length > 1) {
                data.profile_link = subtitles[0].href;
                data.post_link = subtitles[1].href;
            } else if(subtitles.length == 1){
                data.post_link = subtitles[0].href;
                data.profile_link = title.href;
            } else {
                data = null;
                return data;
            }
            if(second_part.children.length > 0 && !second_part.children[0].hasAttribute('id')) {
                if(second_part.children[0].getAttribute('class') != '') {
                    if(second_part.children[0].children[0].querySelector(\":scope > div[dir=auto]\") == null) {
                        return null;
                    }
                    data.text += second_part.children[0].children[0].querySelector(\":scope > div[dir=auto]\").textContent;
                } else {
                    if(second_part.children[0] == null) {
                        return null;
                    }
                    data.text += second_part.children[0].textContent;
                }
            }
            if(second_part.children.length > 1 && !second_part.children[1].hasAttribute('id')) {
                if(second_part.children[1].getAttribute('class') != '') {
                    if(second_part.children[1].children[0].querySelector(\":scope > div[dir=auto]\") == null) {
                        return null;
                    }
                    data.text += second_part.children[1].children[0].querySelector(\":scope > div[dir=auto]\").textContent;
                } else {
                    if(second_part.children[1] == null) {
                        return null;
                    }
                    data.text += second_part.children[1].textContent;
                }
            }

            return data;
        })
        all_data = all_data.filter(item => {
            return item != null;
        });

        let count = all_data.length;
        return {count, sdata: all_data};
        "));
            $sdata = $f['sdata'];
            $count = $f['count'];
        }
        $page->close();
        $xdata = [];
        foreach ($sdata as $item) {
            // console.log("itemmmmmmmmmmmm=", item);
            $page = $browser->newPage();
            $page->goto($item["profile_link"]);

            $city = $page->evaluate(JSFunction::createWithBody("
            let e = document.querySelector(\"div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 x2lah0s x1qughib x6s0dn4 xozqiw3 x1q0g3np'] > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x2lah0s x193iq5w xeuugli']\");
            if(e) {
                e.click();
            }
            let x = document.querySelector(\"div[class='x78zum5 xdt5ytf x1iyjqo2 x1n2onr6'] > div[class='x4k7w5x x1h91t0o x1beo9mf xaigb6o x12ejxvf x3igimt xarpa2k xedcshv x1lytzrv x1t2pt76 x7ja8zs x1n2onr6 x1qrby5j x1jfb8zj'] a\");
            if(x) {
                x.click();
            }
            let y = document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/v3/y5/r/VMZOiSIJIwn.png?_nc_eui2=AeF83LbZBKKn3uYtYjn3OcyGysO07LK9kRPKw7Tssr2RE77QWzkWrBNk-fIyTJluVdn2ltA6S71UD8ATh6jI2L4u']\");
            if(y){
                if(y.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\") == null) {
                    throw new Error('dd5');
                }
                return y.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\").textContent;
            }
            let z = document.querySelector(\"img[src='https://static.xx.fbcdn.net/rsrc.php/v3/yW/r/8k_Y-oVxbuU.png?_nc_eui2=AeFxXyXIUKzKuNmXH1nUn0XWAMO_1wPOzQ4Aw7_XA87NDhIikw3fYmgZgfgWF15jqCf6YZw9AnJZxaSSvinlQqO9']\");
            if(z) {
                if(z.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\") == null) {
                    throw new Error('dd6');
                }
                return z.parentElement.parentElement.querySelector(\":scope > div[class='x9f619 x1n2onr6 x1ja2u2z x78zum5 xdt5ytf x193iq5w xeuugli x1r8uery x1iyjqo2 xs83m0k xamitd3 xsyo7zv x16hj40l x10b6aqq x1yrsyyn']\").textContent;
            }
            return '';
        "));
            $page->close();
            $xdata[] = [
                ...$item,
                "city" => $city
            ];
        }
        $browser->close();
        return $xdata;
    }

}
